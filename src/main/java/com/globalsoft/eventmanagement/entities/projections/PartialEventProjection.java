/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.globalsoft.eventmanagement.entities.projections;

import com.globalsoft.eventmanagement.entities.Event;
import java.time.ZonedDateTime;
import org.springframework.data.rest.core.config.Projection;

/**
 *
 * @introduction Projection is used for representing a view which is sent back
 * as response for a particular request. For a client to use this, the name of
 * the projection will also be passed in the URL (?projection=nameofProjection),
 * if not the regular response would be sent back to the user.
 */
//Mark the class with the @Projection annotation
@Projection(name = "partial", types = {Event.class})
public interface PartialEventProjection {
    
    String getName();
    ZonedDateTime getStartTime(); 
    ZonedDateTime getEndTime(); 
    
}
