/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.globalsoft.eventmanagement.repository;

import com.globalsoft.eventmanagement.entities.Organizer;
import org.springframework.data.repository.CrudRepository;

/**
 *
 * @author adebowale.odulaja
 */
public interface OrganizerRepository extends CrudRepository<Organizer, Long> {
    
}
