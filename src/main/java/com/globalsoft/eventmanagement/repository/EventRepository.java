/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.globalsoft.eventmanagement.repository;

import com.globalsoft.eventmanagement.entities.Event;
import java.time.ZoneId;
import java.util.List;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
//import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.data.repository.query.Param;

/**
 *
 * @author adebowale.odulaja
 */
//To implement paging and sorting, all we need to do is extend the PaginAndSortingRepository instead of CrudRepo
public interface EventRepository extends PagingAndSortingRepository<Event, Long> {
    //To implement custom finder, add a find by method e.g below, then to consume or use from rest client
    //enter the endpoint followed by /search/nameOfMethod?variable=theSearchString
    
    //To enable pagination on this endpoint, use Page instead of List and pass pageable as the second parameter in the method.
    List<Event> findByName(@Param("name") String name);
    //Find By multiple parameters
    Page<Event> findByNameAndZoneId(@Param("name") String name, @Param("zoneId") ZoneId zoneId, Pageable pageable);
}
