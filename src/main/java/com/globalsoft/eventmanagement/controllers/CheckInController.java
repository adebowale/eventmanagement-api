/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.globalsoft.eventmanagement.controllers;

import com.globalsoft.eventmanagement.controllers.exceptions.AlreadyCheckedInException;
import com.globalsoft.eventmanagement.entities.Participant;
import com.globalsoft.eventmanagement.repository.ParticipantRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.PersistentEntityResource;
import org.springframework.data.rest.webmvc.PersistentEntityResourceAssembler;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author adebowale.odulaja
 */

@RepositoryRestController
@RequestMapping("/events")
public class CheckInController {
    
    private Participant participant;
    @Autowired
    private ParticipantRepository participantRepository;
    
    //In other to return HAL response, we use the class PersistentEntityResourceAssembler as the second param for the checkin method
    @PostMapping("/checkin/{id}")
    public ResponseEntity<PersistentEntityResource> checkIn(@PathVariable Long id, PersistentEntityResourceAssembler assembler){
        
        Optional<Participant> optional_participant = participantRepository.findById(id);
        
        if(optional_participant.isPresent()){
            participant = optional_participant.get();
            if(participant.getCheckedIn()){
                throw new AlreadyCheckedInException();
            }
            participant.setCheckedIn(true);
            participantRepository.save(participant);
        }
        return ResponseEntity.ok(assembler.toResource(participant));
    }
}
