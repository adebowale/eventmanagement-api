/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.globalsoft.eventmanagement.controllers;

import com.globalsoft.eventmanagement.entities.Event;
import com.globalsoft.eventmanagement.repository.EventRepository;
import java.util.Optional;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.rest.webmvc.RepositoryRestController;
import org.springframework.data.rest.webmvc.ResourceNotFoundException;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 *
 * @author adebowale.odulaja
 */

@RepositoryRestController
@RequestMapping("/events")
public class EventKickOffController {
    
    private Event event;
    //Autowire event repository
    @Autowired
    private EventRepository eventRepository;
    
    @PostMapping("/startevent/{id}")
    public ResponseEntity startEvent(@PathVariable Long id){
        //Get the id of the event to be started
        Optional<Event> event_optional = eventRepository.findById(id);
        
        if (!(event_optional.isPresent())){
            throw new ResourceNotFoundException();
        }
        event = event_optional.get();
        event.setStarted(true);
        eventRepository.save(event);
        
        return ResponseEntity.ok("Event "+event.getName()+" has started successfully");
    }
}
