# Event Management API

Event mangement API is an open source API built in the quest of knowing best framework to use when there's need to build a fast reliable and scalabe API within minutes.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

Any IDE of your choice

### Setting up on your local machine

1. Fork the repo
2. Import it into any IDE of choice
3. Run the application and make necessary changes as you deem fit.

### If you only need the endpoints for your use, follow the instructions below
	
	Here are the list of endpoints available and what they do.

## Built With

* Netbeans IDE
* Spring
* Spring Data Rest
* Mysql Database

## Author(s)

* Adebowale Odulaja
	You can hit me up on Twitter @_walegeek


